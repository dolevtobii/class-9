#pragma once

#include <string>
#include <iostream>

template<class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf();
	T getData();
	BSNode* getLeft();
	BSNode* getRight();

	bool search(T val);

	int getHeight();
	int getDepth(const BSNode& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left; // smaller
	BSNode* _right; // bigger

	int _count; //for question 1 part B
	
	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth

};


/*
    function will construct the node
    input: data
    output: none
*/
template<class T>
inline BSNode<T>::BSNode(T data)
{
    this->_data = data;
    this->_left = nullptr;
    this->_right = nullptr;
    this->_count = 1;
}


/*
    function will copy the node
    input: other node
    output: none
*/
template<class T>
inline BSNode<T>::BSNode(const BSNode& other)
{
    this->_data = other._data;
    this->_count = other._count;
    if (other._left != nullptr)
    {
        this->_left = new BSNode(*other._left);
    }
    if (other._right != nullptr)
    {
        this->_right = new BSNode(*other._right);
    }
}


/*
    function will destruct the node
    input: none
    output: none
*/
template<class T>
inline BSNode<T>::~BSNode()
{
    delete this->_left;
    delete this->_right;
}


/*
    function will insert value to the node
    input: value to insert
    output: none
*/
template<class T>
inline void BSNode<T>::insert(T value)
{
    if (value == this->_data)
    {
        this->_count++;
    }
    else
    {
        if (value > this->_data)
        {
            if (this->_right != nullptr)
            {
                this->_right->insert(value);
            }
            else
            {
                this->_right = new BSNode(value);
            }
        }
        else
        {
            if (this->_left != nullptr)
            {
                this->_left->insert(value);
            }
            else
            {
                this->_left = new BSNode(value);
            }
        }
    }
}


/*
    function will copy the node
    input: the node to copy
    output: the copied node
*/
template<class T>
inline BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
    if (this != nullptr)
    {
        this->_data = other._data;
        this->_count = other._count;
        this->_left = new BSNode(*other._left);

        this->_right = new BSNode(*other._right);
    }
    else
    {
        std::cout << "you cant assignment to a null." << std::endl;
    }


    return *this;
}


/*
    function will check if the value is a leaf
    input: none
    output: if the value is a leaf
*/
template<class T>
inline bool BSNode<T>::isLeaf()
{
    bool result = false;
    if (this->_left == nullptr && this->_right == nullptr)
    {
        result = true;
    }
    return result;
}


/*
    function will return the data
    input: none
    output: data
*/
template<class T>
inline T BSNode<T>::getData()
{
    return this->_data;
}


/*
    function will return the left node
    input: none
    output: left node
*/
template<class T>
inline BSNode<T>* BSNode<T>::getLeft()
{
    return this->_left;
}


/*
    function will return the right node
    input: none
    output: right node
*/
template<class T>
inline BSNode<T>* BSNode<T>::getRight()
{
    return this->_right;
}


/*
    function will check if the value is in the tree
    input: the vlaue
    output: if the value in the tree
*/
template<class T>
inline bool BSNode<T>::search(T val)
{
    bool result = false;

    if (this->_data == val)
    {
        result = true;
    }
    else
    {
        if (val > this->_data)
        {
            if (this->_right != nullptr)
            {
                result = this->_right->search(val);
            }
        }
        else
        {
            if (this->_left != nullptr)
            {
                result = this->_left->search(val);
            }
        }
    }

    return result;
}


/*
    function will return the height of the tree
    input: none
    output: the height of the tree
*/
template<class T>
inline int BSNode<T>::getHeight()
{
    int result = 0;

    if (this->_right != nullptr)
    {
        result = this->_right->getHeight();
        result++;
    }
    if (this->_left != nullptr)
    {
        result = this->_left->getHeight();
        result++;
    }

    return result;
}


/*
    function will print the tree
    input: none
    output: none
*/
template<class T>
inline void BSNode<T>::printNodes() const
{
    if (this != nullptr)
    {
        this->_left->printNodes();
        std::cout << this->_data << " " << this->_count << std::endl;
        this->_right->printNodes();
    }
}


//----------------------------------------------------------------
//not finished
template<class T>
inline int BSNode<T>::getDepth(const BSNode& root) const
{
    return this->getCurrNodeDistFromInputNode(&root);
}


template<class T>
inline int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode* node) const
{
    return 0;
}
