#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>


using std::cout;
using std::endl;
using std::string;

int main()
{
	int i = 0;
	const int arr_size = 15;
	int intArr[arr_size] = { 1000, 2, 3, 17, 50, 300, 250, 300, 120, 900, 27, 45, 687, 14, 25};
	string stringArr[arr_size] = { "e", "d", "a", "b", "c", "f", "r", "f", "w", "t", "k", "g", "l", "y", "u"};

	for (i = 0; i < arr_size; i++) 
	{
		cout << stringArr[i] << " ";
	}
	cout << endl;

	for (i = 0; i < arr_size; i++)
	{
		cout << intArr[i] << " ";
	}
	cout << endl;

	BSNode<int>* bInt = new BSNode<int>(intArr[0]);
	for(i = 1; i < arr_size; i++)
	{
		bInt->insert(intArr[i]);
	}
	bInt->printNodes();
	cout << endl;

	BSNode<string>* bString = new BSNode<string>(stringArr[0]);
	for (i = 1; i < arr_size; i++)
	{
		bString->insert(stringArr[i]);
	}
	bString->printNodes();
	cout << endl;

	cout << "Tree height: " << bInt->getHeight() << endl;
	/*cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;*/
	
	
	//cout << bb->search("7") << endl;
	system("pause");
	delete bInt;
	delete bString;

	return 0;
}

