#include "BSNode.h"


/*
    function will construct the node 
    input: data
    output: none
*/
BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
    this->_count = 1;
}


/*
    function will copy the node
    input: node
    output: none
*/
BSNode::BSNode(const BSNode& other) 
{
    this->_data = other._data;
    this->_count = other._count;
    if (other._left != nullptr)
    {
        this->_left = new BSNode(*other._left);
    }
    if (other._right != nullptr)
    {
        this->_right = new BSNode(*other._right);
    }
}



/*
    function will destruct the node
    input: none
    output: none
*/
BSNode::~BSNode()
{
	delete this->_left; 
	delete this->_right;
}


/*
    function will insert value to the node
    input: value to insert
    output: none
*/
void BSNode::insert(std::string value)
{
    if(value == this->_data)
    {
        this->_count++;
    }
    else
    {
        if (value > this->_data)
        {
            if (this->_right != nullptr)
            {
                this->_right->insert(value);
            }
            else
            {
                this->_right = new BSNode(value);
            }
        }
        else
        {
            if (this->_left != nullptr)
            {
                this->_left->insert(value);
            }
            else
            {
                this->_left = new BSNode(value);
            }
        }
    }
}


/*
    function will copy the node
    input: the node to copy
    output: the copied node
*/
BSNode& BSNode::operator=(const BSNode& other)
{
    if (this != nullptr)
    {
        this->_data = other._data;

        this->_left = new BSNode(*other._left);

        this->_right = new BSNode(*other._right);
    }
    else
    {
        std::cout << "you cant assignment to a null." << std::endl;
    }


    return *this;
}


/*
    function will check if the value is a leaf
    input: none
    output: if the value is a leaf
*/
bool BSNode::isLeaf() const
{
    bool result = false;
    if (this->_left == nullptr && this->_right == nullptr)
    {
        result = true;
    }
    return result;
}


/*
    function will return the data
    input: none
    output: data
*/
std::string BSNode::getData() const
{
	return this->_data;
}


/*
    function will return the left node
    input: none
    output: left node
*/
BSNode* BSNode::getLeft() const
{
	return this->_left;
}


/*
    function will return the right node
    input: none
    output: right node
*/
BSNode* BSNode::getRight() const
{
	return this->_right;
}


/*
    function will check if the value is in the tree
    input: the vlaue
    output: if the value in the tree
*/
bool BSNode::search(std::string val) const
{
    bool result = false;

    if(this->_data == val)
    {
        result = true;
    }
    else
    {
        if (val > this->_data)
        {
            if (this->_right != nullptr)
            {
                result = this->_right->search(val);
            }
        }
        else
        {
            if (this->_left != nullptr)
            {
                result = this->_left->search(val);
            }
        }
    }

    return result;
}


/*
    function will return the height of the tree
    input: none
    output: the height of the tree
*/
int BSNode::getHeight() const
{
    int result = 0;

    if(this->_right != nullptr)
    {
        result = this->_right->getHeight();
        result++;
    }
    if(this->_left != nullptr)
    {
        result = this->_left->getHeight();
        result++;
    }
    
    return result;
}


/*
    function will print the tree
    input: none
    output: none
*/
void BSNode::printNodes() const
{
    if (this != nullptr)
    {
        this->_left->printNodes();
        std::cout << this->_data << " " << this->_count << std::endl;
        this->_right->printNodes();
    }
}


//----------------------------------------------------------------
//not finished
int BSNode::getDepth(const BSNode& root) const
{
    return this->getCurrNodeDistFromInputNode(&root);
}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const
{
    int result = 0;
    if (this->_right != nullptr)
    {
        result = this->_right->getCurrNodeDistFromInputNode(node->_right);
        result++;
    }
    if (this->_left != nullptr)
    {
        result = this->_left->getCurrNodeDistFromInputNode(node->_left);
        result++;
    }
   
    return result;
}
