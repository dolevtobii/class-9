#pragma once

#include <string>
#include <iostream>

template<class T>
class Member
{
public:
	Member(T value);
	~Member();
	int compare(T first, T second);
	void bubbleSort(T arr[], int size);
	void printArray(T arr[], int size);

	void setValue(T value);
	T getValue();

	bool operator<(const T other) const;
	T operator=(const T other) const;
	friend std::ostream& operator<<(std::ostream& os, const T temp);

private:
	T _value;
};


/*
	function will construct the node
	input: vlaue
	output: none
*/
template<class T>
inline Member<T>::Member(T value)
{
	this->_value = value;
}


/*
	function will destruct the node
	input: none
	output: none
*/
template<class T>
inline Member<T>::~Member()
{
}


/*
	function will compre two obj
	input: first - the first obj
		   second - the second obj
	output: if they are equal
*/
template<class T>
inline int Member<T>::compare(T first, T second)
{
	int result = 0;

	if (first > second)
	{
		result = -1;
	}
	else if (second > first)
	{
		result = 1;
	}

	return result;
}


/*
	function will sort the array
	input: arr - the array
		   size - the size of the array
	output: none
*/
template<class T>
inline void Member<T>::bubbleSort(T arr[], int size)
{
	int i = 0;
	int k = 0;
	T temp;

	for (i = 0; i < size - 1; i++)
	{
		for (k = i + 1; k < size; k++)
		{
			if (arr[i] > arr[k])
			{
				temp = arr[i];
				arr[i] = arr[k];
				arr[k] = temp;
			}
		}
	}

}


/*
	function will print the array
	input: arr - the array 
		   size - the size of the array
	output: none
*/
template<class T>
inline void Member<T>::printArray(T arr[], int size)
{
	int i = 0;

	for(i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}


/*
	function will set valie
	input: the value
	output: none
*/
template<class T>
inline void Member<T>::setValue(T value)
{
	this->_value = vlaue;
}


/*
	function will return the value
	input: none
	output: the value
*/
template<class T>
inline T Member<T>::getValue()
{
	return this->_value;
}


/*
	function will check if other is smaller than this
	input: other
	output: if other is smaller than this
*/
template<class T>
inline bool Member<T>::operator<(const T other) const
{
	bool result = false
	if (other._value < this->_value)
	{
		result = true;
	}
	return result;
}


/*
	function will set other inside this
	input: other
	output: this
*/
template<class T>
inline T Member<T>::operator=(const T other) const
{
	this->_value = other._value;

	return *this;
}


/*
	function will print the values
	input: os - the ostream
		   temp - the temp obj
	output: osteram
*/
template<class T>
std::ostream& operator<<(std::ostream& os, const T temp)
{
	os << temp._value << endl;
	return os;
}