#include <iostream>
#include "functions.h"


int main() 
{
	Member<double>* m = new Member<double>(0.5);
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << m->compare(1.0, 2.5) << std::endl;
	std::cout << m->compare(4.5, 2.4) << std::endl;
	std::cout << m->compare(4.4, 4.4) << std::endl;

////check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	int i = 0;
	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	m->bubbleSort(doubleArr, arr_size);
	for (i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	m->printArray(doubleArr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}